# Czech translation of NetworkManager-sstp.
# Copyright (C) 2008 the author(s) of NetworkManager-sstp.
# This file is distributed under the same license as the NetworkManager-sstp package.
#
# Jakub Friedl <jfriedl@suse.cz>, 2006.
# Jiří Eischmann <jiri@eischmann.cz>, 2008.
# Zdeněk Hataš <zdenek.hatas@gmail.com>, 2009 - 2016.
# Marek Černocký <marek@manet.cz>, 2021, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: NetworkManager-sstp\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/network-manager-sstp/"
"issues\n"
"POT-Creation-Date: 2022-03-20 21:05+0000\n"
"PO-Revision-Date: 2022-03-24 09:24+0100\n"
"Last-Translator: Marek Černocký <marek@manet.cz>\n"
"Language-Team: Czech <gnome-cs-list@gnome.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Poedit 1.8.7.1\n"

#: ../appdata/network-manager-sstp.metainfo.xml.in.h:1
msgid "SSTP VPN client"
msgstr "Klient SSTP VPN"

#: ../appdata/network-manager-sstp.metainfo.xml.in.h:2
msgid "Client for SSTP virtual private networks"
msgstr "Klient pro virtuální privátní sítě SSTP"

#: ../appdata/network-manager-sstp.metainfo.xml.in.h:3
msgid "Support for configuring SSTP virtual private network connections."
msgstr "Podpora konfigurace připojení virtuálních privátních sítí SSTP."

#: ../appdata/network-manager-sstp.metainfo.xml.in.h:4
msgid ""
"Compatible with servers following the SSTP specification including the "
"Microsoft servers."
msgstr ""
"Kompatibilní se servery splňujícími specifikaci SSTP, včetně těch od firmy "
"Microsoft."

#: ../appdata/network-manager-sstp.metainfo.xml.in.h:5
msgid "The NetworkManager Developers"
msgstr "Vývojáři softwaru NetworkManager"

#: ../auth-dialog/main.c:278
msgid "Authenticate VPN"
msgstr "Autentizovat VPN"

#: ../auth-dialog/main.c:287 ../auth-dialog/main.c:384
msgid "Certificate password:"
msgstr "Heslo k certifikátu:"

#: ../auth-dialog/main.c:293
msgid "_HTTP proxy password:"
msgstr "Heslo k _HTTP proxy:"

#: ../auth-dialog/main.c:370
#, c-format
msgid "Authenticate VPN %s"
msgstr "Autentizovat VPN %s"

#: ../auth-dialog/main.c:377 ../properties/nm-sstp-dialog.ui.h:12
msgid "Password:"
msgstr "Heslo:"

#: ../auth-dialog/main.c:391
msgid "HTTP proxy password:"
msgstr "Heslo k HTTP proxy:"

#: ../auth-dialog/main.c:506
#, c-format
msgid "You need to authenticate to access the Virtual Private Network “%s”."
msgstr "Pro přístup k virtuální privátní síti „%s“ musíte provést autentizaci."

#: ../properties/advanced-dialog.c:235
msgid "All Available (Default)"
msgstr "Vše dostupné (výchozí)"

#: ../properties/advanced-dialog.c:239
msgid "128-bit (most secure)"
msgstr "128 bitů (nejbezpečnější)"

#: ../properties/advanced-dialog.c:248
msgid "40-bit (less secure)"
msgstr "40 bitů (méně bezpečné)"

#: ../properties/advanced-dialog.c:367
msgid "PAP"
msgstr "PAP"

#: ../properties/advanced-dialog.c:380
msgid "CHAP"
msgstr "CHAP"

#: ../properties/advanced-dialog.c:392
msgid "MSCHAP"
msgstr "MSCHAP"

#: ../properties/advanced-dialog.c:404
msgid "MSCHAPv2"
msgstr "MSCHAPv2"

#: ../properties/advanced-dialog.c:416
msgid "EAP"
msgstr "EAP"

#: ../properties/advanced-dialog.c:482
msgid "Don't verify certificate identification"
msgstr "Neověřovat identifikaci certifikátu"

#: ../properties/advanced-dialog.c:490
msgid "Verify subject exactly"
msgstr "Ověřovat právě subjekt"

#: ../properties/advanced-dialog.c:498
msgid "Verify name exactly"
msgstr "Ověřovat právě jméno"

#: ../properties/advanced-dialog.c:506
msgid "Verify name by suffix"
msgstr "Ověřovat podle přípony"

#: ../properties/advanced-dialog.c:556
msgid "TLS 1.2 (Default)"
msgstr "TLS 1.2 (výchozí)"

#: ../properties/advanced-dialog.c:564
msgid "TLS 1.3"
msgstr "TLS 1.3"

#: ../properties/nm-sstp-editor-plugin.c:34
msgid "Secure Socket Tunneling Protocol (SSTP)"
msgstr "Secure Socket Tunneling Protocol (SSTP)"

#: ../properties/nm-sstp-editor-plugin.c:35
msgid "Compatible with Microsoft and other SSTP VPN servers."
msgstr "Kompatibilní se servery SSTP VPN od Microsoftu a ostatních."

#: ../properties/nm-sstp-editor.c:314 ../properties/nm-sstp-editor.c:342
#, c-format
msgid "file is not a certificate"
msgstr "soubor není certifikátem"

#: ../properties/nm-sstp-editor.c:365
#, c-format
msgid "file is not a private key"
msgstr "soubor není soukromým klíčem"

#: ../properties/nm-sstp-editor.c:581
msgid "Certificates (TLS)"
msgstr "Certifikáty (TLS)"

#: ../properties/nm-sstp-editor.c:590
msgid "Password"
msgstr "Heslo"

#: ../shared/nm-utils/nm-shared-utils.c:946
#, c-format
msgid "object class '%s' has no property named '%s'"
msgstr "objektová třída „%s“ nemá vlastnost s názvem „%s„“"

#: ../shared/nm-utils/nm-shared-utils.c:953
#, c-format
msgid "property '%s' of object class '%s' is not writable"
msgstr "vlastnost „%s“ objektové třídy „%s“ není zapisovatelná"

#: ../shared/nm-utils/nm-shared-utils.c:960
#, c-format
msgid ""
"construct property \"%s\" for object '%s' can't be set after construction"
msgstr ""
"konstrukce vlastnosti „%s“ pro objekt „%s“ nemůže být nastavena po vytvoření"

#: ../shared/nm-utils/nm-shared-utils.c:968
#, c-format
msgid "'%s::%s' is not a valid property name; '%s' is not a GObject subtype"
msgstr "„%s::%s“ není název vlastnosti; „%s“ není podtyp objektu GObject"

#: ../shared/nm-utils/nm-shared-utils.c:977
#, c-format
msgid "unable to set property '%s' of type '%s' from value of type '%s'"
msgstr "nelze nastavit vlastnost „%s“ typu „%s“ z hodnoty typu „%s“"

#: ../shared/nm-utils/nm-shared-utils.c:988
#, c-format
msgid ""
"value \"%s\" of type '%s' is invalid or out of range for property '%s' of "
"type '%s'"
msgstr ""
"hodnota „%s“ typu „%s“ je neplatná nebo mimo rozsah vlastnosti „%s“ typu „%s“"

#: ../shared/nm-utils/nm-vpn-plugin-utils.c:69
#, c-format
msgid "unable to get editor plugin name: %s"
msgstr "nelze získat název zásuvného modulu editoru: %s"

#: ../shared/nm-utils/nm-vpn-plugin-utils.c:103
#, c-format
msgid "missing plugin file \"%s\""
msgstr "chybějící soubor zásuvného modulu „%s“"

#: ../shared/nm-utils/nm-vpn-plugin-utils.c:109
#, c-format
msgid "cannot load editor plugin: %s"
msgstr "nelze nahrát zásuvný modul editoru: %s"

#: ../shared/nm-utils/nm-vpn-plugin-utils.c:118
#, c-format
msgid "cannot load factory %s from plugin: %s"
msgstr "nelze nahrát generátor %s ze zásuvného modulu: %s"

#: ../shared/nm-utils/nm-vpn-plugin-utils.c:144
msgid "unknown error creating editor instance"
msgstr "neznámá chyba vytváření instance editoru"

#: ../shared/utils.c:58
msgid "Failed to initialize the crypto engine"
msgstr "Nezdařilo se inicializovat kryptografickou knihovnu"

#: ../shared/utils.c:85
#, c-format
msgid "Failed to get subject name"
msgstr "Selhalo získání jména subjektu"

#: ../shared/utils.c:121 ../shared/utils.c:135
#, c-format
msgid "Failed to load certificate"
msgstr "Selhalo načtení certifikátu"

#: ../shared/utils.c:128
#, c-format
msgid "Failed to initialize certificate"
msgstr "Selhala inicializace certifikátu"

#: ../shared/utils.c:185
#, c-format
msgid "Failed to parse pkcs12 file"
msgstr "Selhalo zpracování souboru pkcs12"

#: ../shared/utils.c:191
#, c-format
msgid "Failed to import pkcs12 file"
msgstr "Selhal import souboru pkcs12"

#: ../shared/utils.c:198
#, c-format
msgid "Failed to initialize pkcs12 structure"
msgstr "Selhala inicializace struktury pkcs12"

#: ../shared/utils.c:205
#, c-format
msgid "Failed to read file"
msgstr "Selhalo čtení souboru"

#: ../shared/utils.c:239
#, c-format
msgid "Failed to decrypt private key"
msgstr "Selhalo dešifrování soukromého klíče"

#: ../shared/utils.c:249
#, c-format
msgid "Failed to initialize private key"
msgstr "Selhala inicializace soukromého klíče"

#: ../shared/utils.c:257
#, c-format
msgid "Failed read file"
msgstr "Selhalo čtení souboru"

#: ../shared/utils.c:307
#, c-format
msgid "invalid delimiter character '%c'"
msgstr "neplatný oddělovací znak „%c“"

#: ../shared/utils.c:314
#, c-format
msgid "invalid non-utf-8 character"
msgstr "neplatný znak nespadající do utf-8"

#: ../shared/utils.c:343
#, c-format
msgid "empty host"
msgstr "prázdný hostitel"

#: ../shared/utils.c:355
#, c-format
msgid "invalid port"
msgstr "neplatný port"

#: ../src/nm-sstp-service.c:236
#, c-format
msgid "invalid gateway “%s”"
msgstr "neplatná brána „%s“"

#: ../src/nm-sstp-service.c:250
#, c-format
msgid "invalid integer property “%s”"
msgstr "neplatná číselná vlastnost „%s“"

#: ../src/nm-sstp-service.c:260
#, c-format
msgid "invalid boolean property “%s” (not yes or no)"
msgstr "neplatná pravdivostní vlastnost „%s“ (není ano či ne)"

#: ../src/nm-sstp-service.c:267
#, c-format
msgid "unhandled property “%s” type %s"
msgstr "neošetřená vlastnost „%s“ typu %s"

#: ../src/nm-sstp-service.c:278
#, c-format
msgid "property “%s” invalid or not supported"
msgstr "vlastnost „%s“ není platná nebo podporovaná"

#: ../src/nm-sstp-service.c:296
msgid "No VPN configuration options."
msgstr "Žádné volby nastavení VPN."

#: ../src/nm-sstp-service.c:316
#, c-format
msgid "Missing required option “%s”."
msgstr "Chybí požadovaná volba „%s“."

#: ../src/nm-sstp-service.c:486
msgid "Could not find sstp client binary."
msgstr "Nelze najít program klienta sstp."

#: ../src/nm-sstp-service.c:496
msgid "Missing VPN gateway."
msgstr "Chybí brána VPN."

#: ../src/nm-sstp-service.c:929
msgid "Could not find the pppd binary."
msgstr "Nelze nalézt program sstp."

#: ../src/nm-sstp-service.c:994 ../src/nm-sstp-service.c:1013
msgid "Missing VPN username."
msgstr "Schází jméno uživatele VPN"

#: ../src/nm-sstp-service.c:1002 ../src/nm-sstp-service.c:1021
msgid "Missing or invalid VPN password."
msgstr "Chybějící nebo neplatné heslo pro VPN."

#: ../src/nm-sstp-service.c:1170
msgid "Invalid or missing SSTP gateway."
msgstr "Neplatná nebo chybějící brána SSTP"

#: ../src/nm-sstp-service.c:1231
msgid ""
"Could not process the request because the VPN connection settings were "
"invalid."
msgstr ""
"Nezdařilo se zpracovat požadavek, protože nastavení připojení VPN bylo "
"neplatné."

#: ../src/nm-sstp-service.c:1266
msgid "Invalid connection type."
msgstr "Neplatný typ připojení"

#: ../src/nm-sstp-service.c:1460
msgid "Don't quit when VPN connection terminates"
msgstr "Neukončovat, když skončí spojení VPN"

#: ../src/nm-sstp-service.c:1461
msgid "Enable verbose debug logging (may expose passwords)"
msgstr "Povolit podrobný ladicí výpis (může prozradit hesla)"

#: ../src/nm-sstp-service.c:1462
msgid "D-Bus name to use for this instance"
msgstr "Název použitý v D-Bus pro tuto instanci"

#: ../src/nm-sstp-service.c:1483
msgid ""
"nm-sstp-service provides integrated SSTP VPN capability (compatible with "
"Microsoft and other implementations) to NetworkManager."
msgstr ""
"nm-sstp-service umožňuje NetworkManageru poskytovat služby pro připojování k "
"serverům VPN s protokolem SSTP (Microsoftu a dalších)."

#: ../properties/nm-sstp-dialog.ui.h:1
msgid "Default"
msgstr "Výchozí"

#: ../properties/nm-sstp-dialog.ui.h:2
msgid "<b>General</b>"
msgstr "<b>Obecné</b>"

#: ../properties/nm-sstp-dialog.ui.h:3
msgid ""
"SSTP server IP or name.\n"
"config: the first parameter of sstp"
msgstr ""
"Název nebo IP SSTP serveru.\n"
"config: první parametr sstp"

#: ../properties/nm-sstp-dialog.ui.h:5
msgid "_Gateway:"
msgstr "_Brána:"

#: ../properties/nm-sstp-dialog.ui.h:6
msgid "Authentication"
msgstr "Autentizace"

#: ../properties/nm-sstp-dialog.ui.h:7
msgid "CA"
msgstr "CA"

#: ../properties/nm-sstp-dialog.ui.h:8
msgid "User"
msgstr "Uživatel"

#: ../properties/nm-sstp-dialog.ui.h:9
msgid "Username:"
msgstr "Uživatelské jméno:"

#: ../properties/nm-sstp-dialog.ui.h:10
msgid ""
"Set the name used for authenticating the local system to the peer to "
"<name>.\n"
"            config: user <name>"
msgstr ""
"Nastavit jméno použité pro ověření lokálního systému vůči protějšku na "
"<název>.\n"
"            config: user <jméno>"

#: ../properties/nm-sstp-dialog.ui.h:13
msgid "Password passed to SSTP when prompted for it."
msgstr "Heslo poskytnuté SSTP pokud o něj požádá."

#: ../properties/nm-sstp-dialog.ui.h:14
msgid "Show password"
msgstr "Zobrazit heslo"

#: ../properties/nm-sstp-dialog.ui.h:15
msgid "NT Domain:"
msgstr "Doména NT:"

#: ../properties/nm-sstp-dialog.ui.h:16
msgid ""
"Append the domain name <domain> to the local host name for authentication "
"purposes.\n"
"            config: domain <domain>"
msgstr ""
"Připojit název domény <doména> k lokálnímu názvu stroje pro účely ověření.\n"
"            config: domain <doména>"

#: ../properties/nm-sstp-dialog.ui.h:18
msgid "Type:"
msgstr "Typ:"

#: ../properties/nm-sstp-dialog.ui.h:19
msgid "Select an authentication mode."
msgstr "Vybrat režim autentizace."

#: ../properties/nm-sstp-dialog.ui.h:20
msgid "Ad_vanced..."
msgstr "P_okročilé…"

#: ../properties/nm-sstp-dialog.ui.h:21
msgid "SSTP Advanced Options"
msgstr "Rozšířené volby SSTP"

#: ../properties/nm-sstp-dialog.ui.h:22
msgid "<b>SSL Tunnel</b>"
msgstr "<b>Tunel SSL</b>"

#: ../properties/nm-sstp-dialog.ui.h:23
msgid "_Verify certificate type and extended key usage"
msgstr "Ověřo_vat typ certifikátu a rozšířené využití klíče"

#: ../properties/nm-sstp-dialog.ui.h:24
msgid ""
"Require that the peer certificate used for outer SSL tunnel was signed with "
"a certificate that has a trusted certificate chain, key usage, and extended "
"key usage based on RFC3280 TLS rules."
msgstr ""
"Vyžaduje, aby byl certifikát protějšku použitý pro vnější tunel SSL podepsán "
"pomocí certifikátu, který má řetězec důvěry certifikátu, vyžití klíče a "
"rozšířené využití klíče v souladu s pravidly RFC3280."

#: ../properties/nm-sstp-dialog.ui.h:25
msgid "_Use TLS hostname extensions"
msgstr "Po_užívat rozšíření názvu hostitele TLS"

#: ../properties/nm-sstp-dialog.ui.h:26
msgid ""
"Use the gateway name as specified for this connection in the hostname "
"extensions in the SSL tunnel"
msgstr ""
"Používat název brány, tak jak je uveden pro toto připojení v rozšířeních "
"názvu hostitele v tunelu SSL."

#: ../properties/nm-sstp-dialog.ui.h:27
msgid "<b>Certificate Revocation</b>"
msgstr "<b>Odvolávání certivikátů</b>"

#: ../properties/nm-sstp-dialog.ui.h:28
msgid "CRL"
msgstr "CRL"

#: ../properties/nm-sstp-dialog.ui.h:29
msgid "Connection"
msgstr "Připojení"

#: ../properties/nm-sstp-dialog.ui.h:30
msgid "<b>Authentication</b>"
msgstr "<b>Autentizace</b>"

#: ../properties/nm-sstp-dialog.ui.h:31
msgid "Allow the following authentication methods:"
msgstr "Povolit následující metody autentizace:"

#: ../properties/nm-sstp-dialog.ui.h:32
msgid ""
"Allow/disable authentication methods.\n"
"config: refuse-pap, refuse-chap, refuse-mschap, refuse-mschap-v2, refuse-eap"
msgstr ""
"Povolit/zakázat metody ověření.\n"
"config: refuse-pap, refuse-chap, refuse-mschap, refuse-mschap-v2, refuse-eap"

#: ../properties/nm-sstp-dialog.ui.h:34
msgid "<b>Security and Compression</b>"
msgstr "<b>Bezpečnost a komprimace</b>"

#: ../properties/nm-sstp-dialog.ui.h:35
msgid "Use _Point-to-Point encryption (MPPE)"
msgstr "Používat _Point-to-Point šifrování (MPPE)"

#: ../properties/nm-sstp-dialog.ui.h:36
msgid ""
"Note: MPPE encryption is only available with MSCHAP authentication methods. "
"To enable this checkbox, select one or more of the MSCHAP authentication "
"methods: MSCHAP or MSCHAPv2."
msgstr ""
"Poznámka: Šifrování MPPE je možné pouze při použití autentizačních metod "
"MSCHAP. Tento přepínač zpřístupníte vybráním jedné nebo více autentizačních "
"metod (MSCHAP nebo MSCHAPv2)."

#: ../properties/nm-sstp-dialog.ui.h:37
msgid "_Security:"
msgstr "_Bezpečnost:"

#: ../properties/nm-sstp-dialog.ui.h:38
msgid ""
"Require the use of MPPE, with 40/128-bit encryption or all.\n"
"config: require-mppe, require-mppe-128 or require-mppe-40"
msgstr ""
"Vyžaduje použití MPPE se 40/128bitovým šifrovnáním nebo vše.\n"
"config: require-mppe, require-mppe-128 nebo require-mppe-40"

#: ../properties/nm-sstp-dialog.ui.h:40
msgid "Allow st_ateful encryption"
msgstr "Povolit st_avové šifrování"

#: ../properties/nm-sstp-dialog.ui.h:41
msgid ""
"Allow MPPE to use stateful mode. Stateless mode is still attempted first.\n"
"config: mppe-stateful (when checked)"
msgstr ""
"Povolit MPPE používat stavový režim. Bezstavový režim je stále zkoušen jako "
"první.\n"
"config: mppe-stateful (pokud je zaškrtnuto)"

#: ../properties/nm-sstp-dialog.ui.h:43
msgid "Allow _BSD data compression"
msgstr "Povolit komprimaci _BSD dat"

#: ../properties/nm-sstp-dialog.ui.h:44
msgid ""
"Allow/disable BSD-Compress compression.\n"
"config: nobsdcomp (when unchecked)"
msgstr ""
"Povolit/zakázat komprimaci BSD.\n"
"config: nobsdcomp (pokud není zaškrtnuto)"

#: ../properties/nm-sstp-dialog.ui.h:46
msgid "Allow _Deflate data compression"
msgstr "Povolit komprimaci dat _Deflate"

#: ../properties/nm-sstp-dialog.ui.h:47
msgid ""
"Allow/disable Deflate compression.\n"
"config: nodeflate (when unchecked)"
msgstr ""
"Povolit/zakázat komprimaci Deflate.\n"
"config: nodeflate (pokud není zaškrtnuto)"

#: ../properties/nm-sstp-dialog.ui.h:49
msgid "Use TCP _header compression"
msgstr "Používat komprimaci _hlavičky TCP"

#: ../properties/nm-sstp-dialog.ui.h:50
msgid ""
"Allow/disable Van Jacobson style TCP/IP header compression in both the "
"transmit and the receive directions.\n"
"config: novj (when unchecked)"
msgstr ""
"Povolit/zakázat Van Jacobsonův styl komprimace hlaviček TCP/IP pro oba směry "
"(odesílání a příjem).\n"
"config: novj (pokud není zaškrtnuto)"

#: ../properties/nm-sstp-dialog.ui.h:52
msgid "<b>Echo</b>"
msgstr "<b>Echo</b>"

#: ../properties/nm-sstp-dialog.ui.h:53
msgid "Send PPP _echo packets"
msgstr "Posílat pakety PPP _echo"

#: ../properties/nm-sstp-dialog.ui.h:54
msgid ""
"Send LCP echo-requests to find out whether peer is alive.\n"
"config: lcp-echo-failure and lcp-echo-interval"
msgstr ""
"Zjišťovat pomocí LCP echo-request zda protějšek žije.\n"
"config: lcp-echo-failure a lcp-echo-interval"

#: ../properties/nm-sstp-dialog.ui.h:56
msgid "<b>Misc</b>"
msgstr "<b>Různé</b>"

#: ../properties/nm-sstp-dialog.ui.h:57
msgid "Use custom _unit number:"
msgstr "Po_užít vlastní číslo zařízení:"

#: ../properties/nm-sstp-dialog.ui.h:58
msgid ""
"Enable custom index for ppp<n> device name.\n"
"config: unit <n>"
msgstr ""
"Povolit vlastní index pro název zařízení ppp<n>.\n"
"config: unit <n>"

#: ../properties/nm-sstp-dialog.ui.h:60
msgid "Point-to-Point"
msgstr "Point-to-Point"

#: ../properties/nm-sstp-dialog.ui.h:61
msgid "<b>Identity</b>"
msgstr "<b>Identita</b>"

#: ../properties/nm-sstp-dialog.ui.h:62
msgid ""
"Use this to override the value specified by the certificate\n"
"\n"
"By default, this will be populated, when correct password for certificate is "
"supplied, with the Microsoft OID for username; or the Subject name of the "
"certificate when this is not available. "
msgstr ""
"Použijte k přepsání hodnoty dané certifikátem.\n"
"\n"
"Pokud není uvdeno, je standardně doplněno pomocí Microsoft OID pro "
"uživatelské jméno nebo pomocí jména Subjektu z certifikátu, a to po té, co "
"je poskytnuto správné heslo k certifikátu,"

#: ../properties/nm-sstp-dialog.ui.h:65
msgid "<b>Certificate Validation</b>"
msgstr "<b>Ověřování certifikátu</b>"

#: ../properties/nm-sstp-dialog.ui.h:66
msgid "_Subject Match"
msgstr "_Shoda subjektu s"

#: ../properties/nm-sstp-dialog.ui.h:67
msgid ""
"Subject or Common Name to verify server certificate information against. "
msgstr ""
"Subjekt nebo CN, vůči kterému se mají údaje serverového certifikátu "
"porovnávat."

#: ../properties/nm-sstp-dialog.ui.h:68
msgid "_Verify Method"
msgstr "Metoda ověřo_vání"

#: ../properties/nm-sstp-dialog.ui.h:69
msgid ""
"Verify server certificate identification.\n"
"\n"
"When enabled, connection will only succeed if the server certificate matches "
"some expected properties.\n"
"Matching can either apply to the whole certificate subject (all the "
"fields),\n"
"or just the Common Name (CN field).\n"
"                                    "
msgstr ""
"Ověřovat identifikaci serverového certifikátu.\n"
"\n"
"Když je zapnuto, bude připojení úspěšné, pouze pokud bude serverový "
"certifikát odpovídat některým očekávaným vlastnostem.\n"
"Kontorla shody se může používat na celý subjekt certifikátu (všechna pole), "
"nebo pouze Běžné jméno (pole CN)."

#: ../properties/nm-sstp-dialog.ui.h:75
msgid ""
"Require that peer certificate was signed with an explicit key usage and "
"extended key usage based on RFC3280 TLS rules."
msgstr ""
"Požadovat po protějšku certifikát, který byl podepsán klíčem s výslovným "
"využitím a klíčem s rozšiřujícím využitím podle pravidel RFC3280."

#: ../properties/nm-sstp-dialog.ui.h:76
msgid "<b>Compatibility</b>"
msgstr "<b>Kompatibilita</b>"

#: ../properties/nm-sstp-dialog.ui.h:77
msgid "_Max TLS Version"
msgstr "_Max. verze TLS"

#: ../properties/nm-sstp-dialog.ui.h:78
msgid ""
"Use this to control the maximum supported EAP-TLS version. Default is TLS "
"1.2, and use of TLS 1.3 is considered experimental at this point."
msgstr ""
"Použijte k omezení podporované verz EAP-TLS. Výchozí je TLS 1.2 a TLS 1.3 je "
"v tuto chvíli považováno za experimentální."

#: ../properties/nm-sstp-dialog.ui.h:79
msgid "TLS Authentication"
msgstr "Autentizace TLS"

#: ../properties/nm-sstp-dialog.ui.h:80
msgid "Address:"
msgstr "Adresa:"

#: ../properties/nm-sstp-dialog.ui.h:81
msgid "Port:"
msgstr "Port:"

#: ../properties/nm-sstp-dialog.ui.h:82
msgid "0"
msgstr "0"

#: ../properties/nm-sstp-dialog.ui.h:83
msgid "Show Password"
msgstr "Zobrazovat heslo"

#: ../properties/nm-sstp-dialog.ui.h:84
msgid "Proxy"
msgstr "Proxy"
